class Temperature
  # TODO: your code goes here!
  def initialize(options ={})
    @options = options
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  attr_accessor :options, :fahrenheit, :celsius

  def in_fahrenheit
    if self.fahrenheit
      fahren = self.fahrenheit
    else fahren = self.celsius*1.8+32
    end
  end

  def in_celsius
    if self.celsius
      cel = self.celsius
    else cel = ((self.in_fahrenheit-32)/1.8).round(1)
    end
  end

  def self.from_celsius(temp)
    return Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    return Temperature.new(f: temp)
  end

end

class Celsius < Temperature
  def initialize(celsius)
    @celsius = celsius
  end

  attr_accessor :celsius
end

class Fahrenheit < Temperature
  def initialize(fahrenheit)
    @fahrenheit = fahrenheit
  end

end
