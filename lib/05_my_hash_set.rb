# MyHashSet
#
# Ruby provides a class named `Set`. A set is an unordered collection of
# values with no duplicates.  You can read all about it in the documentation:
#
# http://www.ruby-doc.org/stdlib-2.1.2/libdoc/set/rdoc/Set.html
#
# Let's write a class named `MyHashSet` that will implement some of the
# functionality of a set. Our `MyHashSet` class will utilize a Ruby hash to keep
# track of which elements are in the set.  Feel free to use any of the Ruby
# `Hash` methods within your `MyHashSet` methods.
#
# Write a `MyHashSet#initialize` method which sets an empty hash object to
# `@store`. Next, write an `#insert(el)` method that stores `el` as a key
# in `@store`, storing `true` as the value. Write an `#include?(el)`
# method that sees if `el` has previously been `insert`ed by checking the
# `@store`; return `true` or `false`.
#
# Next, write a `#delete(el)` method to remove an item from the set.
# Return `true` if the item had been in the set, else return `false`.  Add
# a method `#to_a` which returns an array of the items in the set.
#
# Next, write a method `set1#union(set2)` which returns a new set which
# includes all the elements in `set1` or `set2` (or both). Write a
# `set1#intersect(set2)` method that returns a new set which includes only
# those elements that are in both `set1` and `set2`.
#
# Write a `set1#minus(set2)` method which returns a new set which includes
# all the items of `set1` that aren't in `set2`.

class MyHashSet

  def initialize
    @store = Hash.new
  end

  attr_accessor :store

  def insert(el)
    if self.store.include?(el) == false
      self.store[el]=true
    end
  end

  def include?(el)
    return true if self.store.keys.include?(el)
  end

  def delete(el)
    if self.store.include?(el)
      self.store.delete(el)
      return true
    else return false
    end
  end

  def union(set_two)
    new_set = self.store.keys.push(set_two.store.keys).flatten.uniq
    new_hash = MyHashSet.new()
    new_set.each do |key|
      new_hash.store[key]=true
    end
    return new_hash
  end

  def to_a
    self.store.keys
  end

  def intersect(set_two)
    new_set=[]
    set_two.store.keys.each do |el|
      if self.store.keys.include?(el)
        new_set.push(el)
      end
    end
    new_hash = MyHashSet.new()
    new_set.each do |key|
      new_hash.store[key]=true
    end
    return new_hash
  end

  def minus(set_two)
    new_set=[]
    self.store.keys.each do |el|
      if set_two.store.keys.include?(el) == false
        new_set.push(el)
      end
    end
    new_hash = MyHashSet.new()
    new_set.each do |key|
      new_hash.store[key]=true
    end
    return new_hash
  end

end

# Bonus
#
# - Write a `set1#symmetric_difference(set2)` method; it should return the
#   elements contained in either `set1` or `set2`, but not both!
# - Write a `set1#==(object)` method. It should return true if `object` is
#   a `MyHashSet`, has the same size as `set1`, and every member of
#   `object` is a member of `set1`.
