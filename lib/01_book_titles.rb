class Book
  # TODO: your code goes here!

  attr_accessor :title

  def title=(title)
    words = title.split(" ")
    small_words = "in the of and or if an a".split(" ")
    capitalized = words.each_with_index do |word,idx|
      if idx == 0 || small_words.include?(word) == false
        word.capitalize!
      end
    end
    @title = capitalized.join(" ")
  end
end
