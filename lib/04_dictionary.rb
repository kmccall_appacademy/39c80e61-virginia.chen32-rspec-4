class Dictionary
  # TODO: your code goes here!

  def initialize
    @entries = Hash.new()
  end

  attr_accessor :entries

  def add(entry)
    if entry.instance_of? String
      entries[entry]=nil
    else entries[entry.keys[0]]=entry.values[0]
    end
  end

  def keywords
    entries.keys.sort
  end

  def include?(keyword)
    entries.keys.include?(keyword)
  end

  def find(keyword)
    if entries.length == 0
      return {}
    end
    return entries.select {|k,v| k.include?(keyword)}
  end

  def printable
    printout = ""
    entries.sort.each do |k,v|
      printout << "[#{k}] \"#{v}\"\n"
    end
    printout = printout.chomp
  end

end
