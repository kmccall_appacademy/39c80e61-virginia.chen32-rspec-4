class Timer

  def initialize(seconds=0)
      # save variables...
      @seconds = seconds
  end

  attr_accessor :seconds

  def time_string
    return "00:00:00" if self.seconds == 0
    hour = 0
    min = 0
    sec = self.seconds
    if sec >= 3600
      hour = (sec/3600)
      sec = sec - (hour*3600)
      hour = hour.to_s
      if hour.length == 1
        hour = "0" + hour
      end
    else hour = "00"
    end
    if sec >= 60
      min = sec/60
      sec = sec - (min*60)
      min = min.to_s
      if min.length == 1
        min = "0" + min
      end
    else min = "00"
    end
    sec = sec.to_s
    if sec.length == 1
      sec = "0" + sec
    end
    return "#{hour}:#{min}:#{sec}"
  end


end
